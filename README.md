# jekyll-to-dayone

## Purpose

This script is used to take a [Markdown-formatted](https://en.wikipedia.org/wiki/Markdown) document with [YAML front-matter](https://jekyllrb.com/docs/front-matter/) and import it into Day One while preserving metadata and image content.

## Requirements

- Python 2.7+
- [python-frontmattter](https://pypi.org/project/python-frontmatter/)
- [Day One CLI](http://help.dayoneapp.com/tips-and-tutorials/command-line-interface-cli)

## Limitations

1. The script expects that your post has valid front-matter, specifically that which is valid for use in Jekyll or Octopress.
2. Entry date/time should be in ISO 8601 format. If your seconds have a high degree of precision, the script will be unhappy.
3. Presently the journal name is set to Blog.

## Usage

```
jekyll-to-dayone.py 2018-10-01-a-test-post.markdown
```

## Issues

For issues, please create/update/etc. the [issue tracker on bitbucket](https://bitbucket.org/mattshelton/jekyll-to-dayone/issues).