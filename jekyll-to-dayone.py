#! /usr/bin/python

import sys          # exit codes
import os           # work with files
import math         # Used for rounding up ratings
import re           # Many RegEx used in plugin procesisng
import tempfile     # Store downloaded photos cleanly
import frontmatter  # Parsing YAML front-matter
import urllib       # For retrieving photos via URI
import subprocess   # Call out to DayOne CLI
import argparse     # Command line arguments
import ntpath       # Get the actual path to a passed file, just in case use the OS-agnostic library

# Block quote {% blockquote John Lennon URL_GOES_HERE %} ... {% endblockquote %}
def parse_bq (s):
  s = s.replace("{% ","")
  s = s.replace(" %}","")
  s = s[11:]
  s = s.lstrip()
  by = ""
  source = ""
  title = ""
  FullCiteWithTitle = "(\S.*)\s+(https?:\/\/)(\S+)\s+(.+)"
  FullCite = "(\S.*)\s+(https?:\/\/)(\S+)"
  AuthorTitle = "([^,]+),([^,]+)"
  Author = "(.+)"
  ret = ""

  if len(s) > 1:
    if re.match(FullCiteWithTitle,s):
      p = re.compile(FullCiteWithTitle)
      by = p.match(s).group(1)
      source = p.match(s).group(2)+p.match(s).group(3)
      title = p.match(s).group(4).title()
      ret = "~ **"+by+"**, ["+title+"]("+source+")"
    elif re.match(FullCite,s):
      p = re.compile(FullCite)
      by = p.match(s).group(1)
      source = p.match(s).group(2)+p.match(s).group(3)
      ret = "~ **["+by+"]("+source+")**"
    elif re.match(AuthorTitle,s):
      p = re.compile(AuthorTitle)
      by = p.match(s).group(1)
      title = p.match(s).group(2).title()
      ret = "~ **"+by+"**, "+title
    elif re.match(Author,s):
      p = re.compile(Author)
      by = p.match(s).group(1)
      ret = "~ **"+by+"**"
  return ret


# {% img (left|right|center)? (/assets/img/file.ext|http(s)?://path.to.com/image.ext) (123[ 123]) ('alt text') %}
def img_to_image_list (s):
  global post_photos_list
  s = s.replace("{% img ","")
  s = s.replace(" %}","")
  s = s.lstrip()

  pattern = "(?P<class>\S.*\s+)?(?P<src>(?:https?:\/\/|\/|\S+\/)\S+)(?:\s+(?P<width>\d+))?(?:\s+(?P<height>\d+))?(?P<title>\s+.+)?"
  p = re.compile(pattern)
  source = p.match(s).group('src')

  if source[:4] == "http":
    filename = get_photo(source)
  else:
    filename = "."+source

  post_photos_list+=filename+" "

  return

# Download a photo to a temp file and return that file name
def get_photo (uri):
  photo = tempfile.NamedTemporaryFile(delete=False)
  photo.write(urllib.urlopen(uri).read())
  photo.close()
  return photo.name


# Magic Starts Here...
arg_parser = argparse.ArgumentParser(description='Import a Jekyll-compliant, Markdown-formatted blog post into a Day One journal')
arg_parser.add_argument("entry", type=argparse.FileType("r"), help="This is the path to the entry you want to import.")
arg_parser.add_argument("-j", "--journal", type=str, nargs=1, required=False, help="The name of the Journal. Defaults to 'Blog'")
arg_parser.add_argument("-b", "--backup", type=bool, required=False, help="Keep a backup import file so you can see what I tried to import.")
arg_parser.add_argument("-c", "--clipath", type=str, nargs=1, required=False, help="Specify a path to the Dayone2 CLi. Defaults to /usr/local/bin/dayone2")
args = arg_parser.parse_args()

post_file = args.entry.name

post_journal = "Blog"
if args.journal:
  post_journal = args.journal[0]

post = frontmatter.load(args.entry)

post_title = post['title']
if not post_title:
  arg_parser.print_help()
  raise RuntimeError("The YAML front-matter is missing a title for your posst.")
  sys.exit(2)

post_date = post['date']
date_regex = "[0-9]{4}-[0-9]{2}-[0-9]{2}\s{1}[0-2][0-9]:[0-5][0-9]:[0-5][0-9]\s[-+][0-9]{3,4}$"
if not post_date:
  arg_parser.print_help()
  raise RuntimeError("The YAML front-matter is missing a date for your post.")
  sys.exit(0)
if type(post_date) is not str:
  raise RuntimeError("The YAML front-matter date is improperly formatted. Correct format is YYYY-MM-DD 00:00:00 -000")
  sys.exit(0)
if re.match(date_regex,post_date) is None:
  arg_parser.print_help()
  raise RuntimeError("The YAML front-matter date is improperly formatted. Correct format is YYYY-MM-DD 00:00:00 -000")
  sys.exit(0)

cmd = "/usr/local/bin/dayone2"
if args.clipath:
  if not os.path.isfile(args.clipath):
    raise RuntimeError("Specified path to dayone2 does not exist.")
  cmd=args.clipath

post_category_list = "-t "
post_photos_list = ""
post_temp_file = "./_import_"+ntpath.basename(post_file)
post_output = open(post_temp_file,"w+", 0)
post_output.write("# "+post_title+"\n\n")

print "Importing "+post_file

bq = False
bq_attr = ""
for line in post.content.splitlines():
  if "{%" in line:

    # Block quote {% blockquote John Lennon URL_GOES_HERE %} ... {% endblockquote %}
    if "blockquote" in line:
      if "endblockquote" in line:
        line = "> "+bq_attr+"\n"
        bq = False
        bq_attr = ""
      else:
        bq_attr = parse_bq(line)
        bq = True
        continue

    # {% codeblock lang:vbnet %} .. {% endcodeblock %}
    if "codeblock" in line:
      line = "```\n"

    # {% youtube Tr1qee-bTZI %}
    if "youtube" in line:
      line_parts = line.split(" ")
      line = "https://www.youtube.com/watch?v="+line_parts[2]+"\n"

    # {% vimeo 60185616 %}
    if "vimeo" in line:
      line_parts = line.split(" ")
      line = "https://vimeo.com/"+line_parts[2]+"\n"

    # {% rating 4.0 %}
    if "rating" in line:
      line_parts = line.split(" ")
      rating = line_parts[2]
      count = math.ceil(float(rating))
      new_line = ""
      while count > 0:
        new_line += "&#9733;"
        count -= 1;
      line = new_line+"\n"

    if "img" in line:
      img_to_image_list(line)
      line = ""

    if "flickr" in line:
      flickr_to_image_list(line)
      line = ""

  if bq:
    line = "> "+line
  line = line+"\n"
  post_output.write(line.encode("utf-8"))

if post_photos_list:
  photo_string = "-p "+post_photos_list
else:
  photo_string = ""

if post['categories']:
  for c in post['categories']:
    c = c.replace(" ","\ ")
    c+=" "
    post_category_list+=c
else:
  post_category_list=""

post_output.close()

command=cmd+" --date='"+post_date+"' -j "+post_journal+" "+photo_string+post_category_list+"-- new < "+post_temp_file
#print command
subprocess.call(command, shell=True)

if not args.backup:
  os.remove(post_temp_file)
